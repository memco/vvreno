/* Use this script if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'fontawesome\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-warning-sign' : '&#xf071;',
			'icon-volume-up' : '&#xf028;',
			'icon-volume-down' : '&#xf027;',
			'icon-volume-off' : '&#xf026;',
			'icon-headphones' : '&#xf025;',
			'icon-envelope' : '&#xf003;',
			'icon-download-alt' : '&#xf019;',
			'icon-ok' : '&#xf00c;',
			'icon-step-backward' : '&#xf048;',
			'icon-fast-backward' : '&#xf049;',
			'icon-backward' : '&#xf04a;',
			'icon-play' : '&#xf04b;',
			'icon-pause' : '&#xf04c;',
			'icon-stop' : '&#xf04d;',
			'icon-forward' : '&#xf04e;',
			'icon-fast-forward' : '&#xf050;',
			'icon-step-forward' : '&#xf051;',
			'icon-chevron-right' : '&#xf054;',
			'icon-chevron-left' : '&#xf053;',
			'icon-plus-sign' : '&#xf055;',
			'icon-minus-sign' : '&#xf056;',
			'icon-remove-sign' : '&#xf057;',
			'icon-ok-sign' : '&#xf058;',
			'icon-question-sign' : '&#xf059;',
			'icon-exclamation-sign' : '&#xf06a;',
			'icon-info-sign' : '&#xf05a;',
			'icon-calendar' : '&#xf073;',
			'icon-twitter-sign' : '&#xf081;',
			'icon-facebook-sign' : '&#xf082;',
			'icon-pushpin' : '&#xf08d;',
			'icon-google-plus-sign' : '&#xf0d4;',
			'icon-dashboard' : '&#xf0e4;',
			'icon-double-angle-right' : '&#xf101;',
			'icon-double-angle-left' : '&#xf100;',
			'icon-angle-left' : '&#xf104;',
			'icon-angle-right' : '&#xf105;',
			'icon-trash' : '&#xf014;',
			'icon-time' : '&#xf017;',
			'icon-cog' : '&#xf013;',
			'icon-home' : '&#xf015;',
			'icon-map-marker' : '&#xf041;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; i < els.length; i += 1) {
		el = els[i];
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};