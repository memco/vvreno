<?php

class Tweets {

	/**
	 * @param integer amount number of tweets to grab
	 */
	public static function getTweets($amount=10) {
		Twitter::$cacheDir = path('storage').'cache/tweets';

		$twitter = new Twitter(config::get('twitter.consumer_key'), config::get('twitter.consumer_secret'), config::get('twitter.access_token'), config::get('twitter.access_secret'));

		$statuses = $twitter->load(Twitter::ME,$amount,array('screen_name'=>'TheViewReno'));

		// Linkify text of tweets
		foreach($statuses as $status) {
			$status = Twitter::clickable($status);
		}

		return $statuses;
	}

}