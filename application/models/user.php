<?php

class User extends Eloquent {

	/**
	 * Make the created_at date a formattable DateTime object.
	 */
	public function get_created_at() {
	    $date = $this->get_attribute('created_at');
	    if (is_string($date)) return DateTime::createFromFormat('Y-m-d H:i:s', $date);
	    return $date;
	}

	public function get_updated_at() {
	    $date = $this->get_attribute('updated_at');
	    if (is_string($date)) return DateTime::createFromFormat('Y-m-d H:i:s', $date);
	    return $date;
	}

	public function set_password($password) {
		if(empty($password) && ! $this->exists) {
			$pass = Str::random(12);
			$password = $pass;
			Alert::add('notice',"The new password is: $pass");
		}
		$this->set_attribute('password',Hash::make($password));
	}


	public static function validate_login($data) {
		$rules = array(
			'username' => 'required',
			'password' => 'required'
		);

		$messages = array(
		);

		return Validator::make($data, $rules, $messages);
	}

	public static function validate_user_create($data) {

		$rules = array(
			'username' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'confirmed'
		);

		$messages = array(
			'username_required' => 'The <a href="#:attribute">:attribute</a> field is required.',
			'email_required' => 'The <a href="#:attribute">:attribute</a> field is required.',
			'email_email' => 'The <a href="#:attribute">:attribute</a> does not appear to be a valid address.',
			'email_unique' => 'The <a href="#:attribute">:attribute</a> address is already registered.',
			'password_confirmed' => 'The <a href="#:attribute">password fields</a> do not match.'
		);

		return Validator::make($data, $rules, $messages);
	}

	public static function validate_user_update($data, $id) {

		$rules = array(
			'username' => 'required',
			'email' => 'required|email|unique:users,email,'.$id,
			'password' => 'confirmed'
		);

		$messages = array(
			'username_required' => 'The <a href="#:attribute">:attribute</a> field is required.',
			'email_required' => 'The <a href="#:attribute">:attribute</a> field is required.',
			'email_email' => 'The <a href="#:attribute">:attribute</a> does not appear to be a valid address.',
			'email_unique' => 'The <a href="#:attribute">:attribute</a> address is already registered.',
			'password_confirmed' => 'The <a href="#:attribute">password fields</a> do not match.'
		);

		return Validator::make($data, $rules, $messages);
	}
}