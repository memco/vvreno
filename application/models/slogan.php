<?php

class Slogan extends Eloquent {
    public function get_created_at() {
        if(! $this->get_attribute('created_at') instanceof DateTime)
        {
            return new DateTime($this->get_attribute('created_at'));
        }
    }

    public function set_created_at($date) {
        if(is_null($date))
        {
            return new DateTime;
        }
    }

    public function get_updated_at() {
        if(! $this->get_attribute('updated_at') instanceof DateTime)
        {
            return new DateTime($this->get_attribute('updated_at'));
        }
    }

    public static function validate($data) {
        $rules = array();

        $messages = array();

        return Validator::make($data, $rules, $messages);
    }
}