<?php
// namespace Alerts;
class Alert
{
	private static $session_variable = 'alerts';
	private static $messages = array();

	public static function serialize_messages()
	{
		if( !empty( static::$messages ))
		{
			\Session::flash( static::$session_variable, serialize( static::$messages ));
		}
	}
	public static function unserialize_messages()
	{
		if( empty( static::$messages ))
		{
			if( \Session::has( static::$session_variable ))
			{
				$unserialized = unserialize( \Session::get( static::$session_variable ));
				static::$messages = array_merge( $unserialized, static::$messages );
			}
		}
	}
	public static function add( $key, $message, $section = 'default' )
	{
		// if section doesn't exist
		if( !isset( static::$messages[$section] ))
		{
			static::$messages[$section] = new \Laravel\Messages;
		}
		// add message to the section
		static::$messages[$section]->add( $key, $message );
	}
	public static function has( $type, $section = 'default' )
	{
		static::unserialize_messages();
		if( isset( static::$messages[$section] ))
		{
			return static::$messages[$section]->has( $type );
		}
		return false;
	}
	public static function render( $view = 'alerts.default', $section = 'default' )
	{
		static::unserialize_messages();
		if( !isset( static::$messages[$section] ))
		{
			return '';
		}
		return \View::make( $view )->with( 'messages', static::$messages[$section] )->render();
	}
	public static function test() {
		static::add('success','Test success message');
		static::add('warning','Test warning message');
		static::add('notice','Test notice message');
		static::add('error','Test error message.<br>So many things were wrong it goes over multiple lines.');
		static::add('error','…And there\'s two of them.');
	}
}