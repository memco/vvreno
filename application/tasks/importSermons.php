<?php

class ImportSermons_Task {
  public function run()
  {
    echo "Starting import";
    foreach(Feed::get_sermon_net_sermons()->get_items() as $item) {
      echo "\nNew item";

      if(! Sermon::where_url($item->get_permalink())->first()) {
        echo "\nNo record in the database. Adding…";
        $sermon = new Sermon;
        $sermon->title = str_replace(' - Audio', '', $item->get_title());
        $authors = array();
        foreach($item->get_authors() as $author) {
          if(! is_null($name = $author->get_name()))
          $authors[] = $name;
        }
        $sermon->speakers = implode(',',$authors);
        $sermon->url = $item->get_permalink();
        $sermon->preached_on = new DateTime($item->get_date());

        $enclosure = $item->get_enclosure();

        $sermon->size = round($enclosure->get_length()/1048576,1)." MB";
        $sermon->length = $enclosure->get_duration();
        $sermon->source = 'sermon.net';

        $sermon->save();
      }
    }
  }
}