<?php

class Sermon_Controller extends Base_Controller {

	public $restful = true;

	public function get_index()
	{
		$per_page = Input::get('items_per_page', 20);

		if(!Auth::user())
		{
			$sermons = Sermon::order_by('preached_on','DESC')
				->where('published','=','1')
				->paginate($per_page);
		}
		else
		{
			$sermons = Sermon::order_by('preached_on','DESC')
				->paginate($per_page);
		}

		// View all
		return View::make('layout.index')
			->with('content', View::make('sermon.index')
				->with('sermons',$sermons));
	}

	public function get_view($id) {
		if (! $sermon = Sermon::find($id)) {
			Alert::add('error','Item not found: invalid sermon ID.');
			return Redirect::to_action('sermon@index');
		} else {
			return View::make('layout.index')
				->with('content',View::make('sermon.view')
					->with('sermon', $sermon));
		}
	}

	public function get_add() {
		return View::make('layout.index')
			->with('content',View::make('sermon.add'));
	}

	public function post_add()
	{
		// Add new
		$post = Input::all();

		$validation = Sermon::validate_audio_file($post);

		$file=Input::file('file');

		if($validation->fails()) {
			Log::error("Failed to upload a file {$file['name']}");
			return Redirect::back()
				->with_errors($validation)
				->with_input();
		} else {
			if($sermon = Sermon::add($post['file'])) {
				Alert::add('success','File uploaded!  Please add details below.');
				return Redirect::to_action('sermon@edit',array($sermon->id));
			} else {
				Alert::add('error','File was not added to the database.');
				return Redirect::to_action('sermon@add');
			}
		}
	}

	public function get_edit($id)
	{
		// Validate resource exists
		if (! $sermon = Sermon::find($id)) {
			Alert::add('error','Item not found: invalid sermon ID.');
			return Redirect::to_action('sermon@index');
		} else {
			return View::make('layout.index')
				->with('content',View::make('sermon.edit')
					->with('sermon',$sermon));
		}
	}

	public function put_edit($id) {
		$post = Input::all();

		$validation = Sermon::validate_metadata(Input::all());

		if($validation->fails()) {
			return Redirect::back()
				->with_errors($validation)
				->with_input();
		} else {
			Sermon::update($id,$post);
			Alert::add('success','Details saved.');
			return Redirect::to_action('sermon@view',array($id));
		}
	}

	public function get_delete($id)
	{
		// Validate resource exists
		if (! $sermon = Sermon::find($id)) {
			Alert::add('error','Item not found: invalid sermon ID.');
			return Redirect::to_action('sermon@index');
		} else {
			Alert::add('warning','You are about to permanently delete this sermon and all associated data.  Are you sure you want to delete it?');
			return View::make('layout.index')
				->with('content',View::make('sermon.delete'));
		}
	}

	public function delete_delete($id)
	{
		Sermon::find($id)->delete();
		Alert::add('success','The sermon was successfully deleted');
		return Redirect::to_action('sermon@index');
	}

}