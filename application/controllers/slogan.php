<?php

class Slogan_Controller extends Base_Controller {

    public function get_index()
    {
        // View all
        return View::make('layout.index')
            ->with('content', View::make('slogan.index')
                ->with('slogan',Slogan::order_by('created_at')->first()));
    }

    public function get_view($id) {
        if (! $slogan = Slogan::find($id)) {
            Alert::add('error','Item not found: invalid slogan ID.');
            return View::make('layout.index')
                ->with('content',View::make('slogan.index'));
        } else {
            return View::make('layout.index')
                ->with('content',View::make('slogan.view')
                    ->with('slogan', $slogan));
        }
    }

    public function get_add() {
        return View::make('layout.index')
            ->with('content',View::make('slogan.add'));
    }

    public function post_add()
    {
        // Add new
        $post = Input::all();

        $validation = Slogan::validate($post);

        if($validation->fails()) {
            Alert::add('error','Please correct the errors below and resubmit');
            return Redirect::back()
                ->with_errors($validation)
                ->with_input();
        } else {
            if($slogan = Slogan::create(array_except($post,'csrf_token'))) {
                Alert::add('success','Slogan added.');
                return Redirect::to('/');
            } else {
                Alert::add('error','An error occurred while trying to save the slogan. Please try again. If the error persists, please contact the site administrator.');
                return Redirect::to_action('slogan@add')
                    >with_input();
            }
        }
    }

    public function get_edit($id)
    {
        // Validate resource exists
        if (! $slogan = Slogan::find($id)) {
            return Redirect::to_action('slogan@add');
        } else {
            return View::make('layout.index')
                ->with('content',View::make('slogan.edit')
                    ->with('slogan',$slogan));
        }
    }

    public function put_edit($id) {
        $post = Input::all();

        $validation = Slogan::validate($post);

        if($validation->fails()) {
            Alert::add('error','Please correct the errors below and resubmit');
            return Redirect::back()
                ->with_errors($validation)
                ->with_input();
        } else {
            Slogan::update($id,array_except($post,'csrf_token'));
            Alert::add('success','Details saved.');
            return Redirect::to('/');
        }
    }

    public function get_delete($id)
    {
        // Validate resource exists
        if (! $slogan = Slogan::find($id)) {
            Alert::add('error','Item not found: invalid slogan ID.');
            return View::make('layout.index')
                ->with('content',View::make('slogan.index'));
        } else {
            Alert::add('warning','You are about to permanently delete this slogan and all associated data.  Are you sure you want to delete it?');
            return View::make('layout.index')
                ->with('content',View::make('slogan.delete'));
        }
    }

    public function delete_delete($id)
    {
        Slogan::find($id)->delete();
        Alert::add('success','The slogan was successfully deleted');
        return Redirect::to_action('slogan@index');
    }

}