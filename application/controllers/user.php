
	<?php

class User_Controller extends Base_Controller {

	public $restful = true;

	public function get_index() {
		return View::make('layout.index')
		 	->with('title','Users')
		 	->with('content',View::make('user.index')
		 		->with('users',User::all()));
	}

	public function get_add() {
		return View::make('layout.index')
		 	->with('title','Create new user')
		 	->with('content',View::make('user.add'));
	}

	public function post_add() {
		// Add new
		$validation = User::validate_user_create(Input::all());

		$post = Input::except(array('csrf_token','password_confirmation'));

		if($validation->fails()) {

			return Redirect::back()
				->with_errors($validation)
				->with_input();
		} else {
			if(User::create($post)) {
				Alert::add('success','Created new user.');
				return Redirect::to_action('user@index');
			} else {
				Alert::add('error','Unable to save new user. Please try again.');
				return Redirect::to_action('user@add')
					->with_input();
			}
		}
	}

	public function get_view($id) {
		return View::make('layout.index')
		 	->with('title','Users')
		 	->with('content',View::make('user.index')
		 		->with('users',User::find($id)));
	}

	public function get_edit($id) {
		if(! $user = User::find($id)) {
			Alert::add('error','Could not find user');
			return Redirect::to_action('user@index');
		}

		if(Auth::user() != $user && Auth::user()->username != "admin")
		{
			Alert::add('error','You can only edit your own account.');
			return Redirect::to_action('user@index');
		}

		return View::make('layout.index')
			->with('title', 'Edit user')
			->with('content', View::make('user.edit')
				->with('user', $user));
	}

	public function put_edit($id)
	{
		if(! $user = User::find($id)) {
			Alert::add('error','Could not find user');
			return Redirect::to_action('user@index');
		}

		if(Auth::user() != $user && Auth::user()->username != "admin")
		{
			Alert::add('error','You can only edit your own account.');
			return Redirect::to_action('user@index');
		}

		$validation = User::validate_user_update(Input::all(), $id);

		$post = Input::except(array('csrf_token','_method','password_confirmation'));

		if($validation->fails()) {
			$this->prepare_errors($validation);

			return Redirect::back()
				->with_errors($validation)
				->with_input();
		}

		if(! User::update($id,$post))
		{
			Alert::add('error','Unable to save new user. Please try again.');
			return Redirect::to_action('user@add')
				->with_input();
		}

		Alert::add('success','Updated user.');
			return Redirect::to_action('user@index');
	}

	public function get_delete($id) {
		if(! $user = User::find($id)) {
			Alert::add('error','Could not find user');
			return Redirect::to_action('user@index');
		} else {
			return View::make('layout.index')
				->with('title', 'Delete user')
				->with('content', View::make('user.delete'));
		}
	}

	public function delete_delete($id) {
		User::find($id)->delete();
		Alert::add('success','The user was successfully deleted');
		return Redirect::to_action('user@index');
	}

	public function get_login() {
		if(Auth::user()) {
			Alert::add('notice','You are already logged in.');
			return Redirect::back();
		}

		// Store where the user came from to send them back after logging in.
		if(Request::referrer() !== URL::to_action('@userlogin') && Request::referrer() !== URL::to_action('user@logout'))
			Session::put('destination',Request::referrer());

		return View::make('layout.index')
		 	->with('title','Login')
		 	->with('content',View::make('login'));
	}

	public function post_login() {
		if(Auth::attempt(array(
			'username' => Input::get('username'),
			'password' => Input::get('password'))))
		{
			Alert::add('success','You are now logged in.');
	    return Redirect::to(Session::get('destination'));
		}
		else
		{
			Alert::add('error','Invalid email or password');
	    return Redirect::to_action('user@login');
		}
	}

	public function get_logout() {
	    Auth::logout();
	    Alert::add('notice','You are now logged out.');
	    return Redirect::to('/');
	}
}