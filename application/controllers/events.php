<?php

class Events_Controller extends Base_Controller {

	public $restful = true;

	public function get_index()
	{
		// View all
		return View::make('layout.index')
			->with('content', View::make('events.index')
				->with('events',Events::order_by('event_date')->get())
			);
	}

	public function get_view($id) {
		if (! $event = Events::find($id)) {
			Alert::add('error','Item not found: invalid event ID.');
			return View::make('layout.index')
				->with('content',View::make('events.index'));
		} else {
			return View::make('layout.index')
				->with('content',View::make('events.view')
					->with('event', $event));
		}
	}

	public function get_add() {
		return View::make('layout.index')
			->with('content',View::make('events.add'));
	}

	public function post_add()
	{
		// Add new
		$post = Input::all();

		$validation = Events::validate($post);

		if($validation->fails()) {
			Alert::add('error','Please correct the errors below and resubmit');
			return Redirect::back()
				->with_errors($validation)
				->with_input();
		} else {
			if($event = Events::create(array_except($post,'csrf_token'))) {
				Alert::add('success','Event added.');
				return Redirect::to_action('events@view', array($event->id));
			} else {
				Alert::add('error','An error occurred while trying to save the event. Please try again. If the error persists, please contact the site administrator.');
				return Redirect::to_action('events@add')
					>with_input();
			}
		}
	}

	public function get_edit($id)
	{
		// Validate resource exists
		if (! $event = Events::find($id)) {
			Alert::add('error','Item not found: invalid event ID.');
			return View::make('layout.index')
				->with('content',View::make('events.index'));
		} else {
			return View::make('layout.index')
				->with('content',View::make('events.edit')
					->with('event',$event));
		}
	}

	public function put_edit($id) {
		$post = Input::all();

		$validation = Events::validate($post);

		if($validation->fails()) {
			Alert::add('error','Please correct the errors below and resubmit');
			return Redirect::back()
				->with_errors($validation)
				->with_input();
		} else {
			Events::update($id,$post);
			Alert::add('success','Details saved.');
			return Redirect::to_action('events@view', array($id));
		}
	}

	public function get_delete($id)
	{
		// Validate resource exists
		if (! $event = Events::find($id)) {
			Alert::add('error','Item not found: invalid event ID.');
			return View::make('layout.index')
				->with('content',View::make('events.index'));
		} else {
			Alert::add('warning','You are about to permanently delete this event and all associated data.  Are you sure you want to delete it?');
			return View::make('layout.index')
				->with('content',View::make('events.delete'));
		}
	}

	public function delete_delete($id)
	{
		Events::find($id)->delete();
		Alert::add('success','The event was successfully deleted');
		return Redirect::to_action('events@index');
	}

}