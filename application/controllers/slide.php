<?php

class Slide_Controller extends Base_Controller {

	public $restful = true;

	public function get_index()
	{

		// View all
		return View::make('layout.index')
			->with('content', View::make('slide.index')
				->with('slides', Slide::get()));
	}

  public function get_add()
  {
    // Create new
    return View::make('layout.index')
      ->with('title','New slide')
      ->with('content', View::make('slide.add'));
  }

  public function get_view($id)
  {
    if(! $slide = Slide::find($id)) {
      Alert::add('error', 'Slide not found.');
      return Redirect::to_action('slide@index', null, 404);
    }

    return View::make('layout.index')
      ->with('title', "Viewing $slide->heading")
      ->with('content', View::make('slide.index')
          ->with('slides', array($slide)));
  }

  public function post_add()
  {
    // Add new
    $post = Input::all();

    $validation = Slide::validate_create($post);

    if($validation->fails()) {
      $this->prepare_errors($validation);
      return Redirect::back()
        ->with_errors($validation)
        ->with_input();
    } else {
      if($slide = Slide::create($post)) {
        Alert::add('success','File uploaded!');
        return Redirect::to_action('slide@view',array($slide->id));
      } else {
        Alert::add('warning','Sorry, we tried to save the data, but it failed. Please try again.');
        return Redirect::to_action('slide@add', null, 500)
          ->with_input();
      }
    }
  }

  public function get_edit($id)
  {
    // Validate resource exists
    if (! $slide = Slide::find($id)) {
      Alert::add('error','Item not found: invalid slide ID.');
      return Redirect::to_action('slide@index',404);
    } else {
      return View::make('layout.index')
        ->with('content',View::make('slide.edit')
          ->with('slide',$slide));
    }
  }

  public function put_edit($id) {
    $post = Input::all();

    $validation = Slide::validate_edit(Input::all());

    if($validation->fails()) {
      $this->prepare_errors($validation);
      return Redirect::back()
        ->with_errors($validation)
        ->with_input();
    } else {
      Slide::update($id,array_except($post,array('csrf_token')));
      Alert::add('success','Details saved.');
      return Redirect::to_action('slide@view', array($id));
    }
  }

  public function get_delete($id)
  {
    // Validate resource exists
    if (! $slide = Slide::find($id)) {
      Alert::add('error','Item not found: invalid slide ID.');
      return View::make('layout.index')
        ->with('content',View::make('slide.index'));
    } else {
      Alert::add('warning','You are about to permanently delete this slide and all associated data.  Are you sure you want to delete it?');
      return View::make('layout.index')
        ->with('content',View::make('slide.delete'));
    }
  }

  public function delete_delete($id)
  {
    slide::find($id)->delete();
    Alert::add('success','The slide was successfully deleted');
    return Redirect::to_action('slide@index');
  }
}