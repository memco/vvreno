<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

// Static pages only use routes
Route::get('/', function()
{
	return View::make('layout.index')
		->with('title','Home')
		->with('content',View::make('home'));
});

Route::get('info', function() {
	return phpinfo();
});

Route::get('documents/cr-8-principles-revised.pdf', function()
{
	$path = path('public').'documents/cr-8-principles-revised.pdf';

	$headers = [
		'X-Sendfile' =>path('public').'documents/cr-8-principles-revised.pdf',
		'Content-Type' => 'application/pdf',
		'Content-Length' => filesize($path),
	];

	$file = fopen($path,'rb');
	return Response::make(fpassthru($file),200,$headers);
	fclose($file);
});

Route::get('about', function()
{
	return View::make('layout.index')
		->with('title','About Us')
		->with('content',View::make('about'));
});

Route::get('pastors', function()
{
	return Redirect::to('about');
});

Route::get('divorce-care', function()
{
	return View::make('layout.index')
		->with('title', 'Divorce Care')
		->with('content', View::make('divorce-care'));
});

Route::get('celebrate-recovery', function()
{
	return View::make('layout.index')
		->with('title', 'Celebrate Recovery')
		->with('content', View::make('celebrate-recovery'));
});

Route::get('baptism', function()
{
	return View::make('layout.index')
		->with('title', 'Baptism')
		->with('content', View::make('baptism'));
});

Route::get('giving', function(){
	return View::make('layout.index')
		->with('title', 'Giving')
		->with('content', View::make('giving'));
});

// Non-static pages; using controllers
// Sermons
Route::any('sermons',array('as'=>'sermons','uses'=>'sermon@index'));
Route::any('sermons/create', array('before'=>'auth','as'=>'sermons/create','uses'=>'sermon@add'));
Route::any('sermons/(:num)/show',array('as'=>'sermons/show','uses'=>'sermon@view'));
Route::any('sermons/(:num)/edit', array('before'=>'auth','as'=>'sermons/edit','uses'=>'sermon@edit'));
Route::any('sermons/(:num)/delete', array('before'=>'auth','as'=>'sermons/delete','uses'=>'sermon@delete'));

// Events
Route::any('events',array('as'=>'events','uses'=>'events@index'));
Route::any('events/create', array('before'=>'auth','as'=>'events/create','uses'=>'events@add'));
Route::any('events/(:num)/show',array('as'=>'events/show','uses'=>'events@view'));
Route::any('events/(:num)/edit', array('before'=>'auth','as'=>'events/edit','uses'=>'events@edit'));
Route::any('events/(:num)/delete', array('before'=>'auth','as'=>'events/delete','uses'=>'events@delete'));

// Slides
Route::any('slides',array('before'=>'auth','as'=>'slides','uses'=>'slide@index'));
Route::any('slides/create', array('before'=>'auth','as'=>'slides/create','uses'=>'slide@add'));
Route::any('slides/(:num)/show',array('before'=>'auth','as'=>'slides/show','uses'=>'slide@view'));
Route::any('slides/(:num)/edit', array('before'=>'auth','as'=>'slides/edit','uses'=>'slide@edit'));
Route::any('slides/(:num)/delete', array('before'=>'auth','as'=>'slides/delete','uses'=>'slide@delete'));

// Slogans
Route::any('slogans',array('before'=>'auth','as'=>'slogans','uses'=>'slogan@index'));
Route::any('slogans/create', array('before'=>'auth','as'=>'slogans/create','uses'=>'slogan@add'));
Route::any('slogans/(:num)/show',array('before'=>'auth','as'=>'slogans/show','uses'=>'slogan@view'));
Route::any('slogans/(:num)/edit', array('before'=>'auth','as'=>'slogans/edit','uses'=>'slogan@edit'));
Route::any('slogans/(:num)/delete', array('before'=>'auth','as'=>'slogans/delete','uses'=>'slogan@delete'));

// Users
Route::any('users',array('before'=>'auth','as'=>'users','uses'=>'user@index'));
Route::any('users/create', array('before'=>'auth','as'=>'users/create','uses'=>'user@add'));
Route::any('users/(:num)/show',array('before'=>'auth','as'=>'users/show','uses'=>'user@view'));
Route::any('users/(:num)/edit', array('before'=>'auth','as'=>'users/edit','uses'=>'user@edit'));
Route::any('users/(:num)/delete', array('before'=>'auth','as'=>'users/delete','uses'=>'user@delete'));
Route::any('login',array('as'=>'login','uses'=>'user@login'));
Route::any('logout',array('as'=>'logout','uses'=>'user@logout'));

Route::get('gcal', function()
{
	var_dump(Gcal::get());
	die;
});

// VIEW COMPOSERS
View::composer('layout.index', function($view)
{

	// Pass in a user if they're logged in to avoid repeated calls
	if($user = Auth::user())
	{
		$view->with('currentUser',$user);

		$admin_menu = Menu::handler('menu-admin')
			->add('events','Events', Menu::items()
				->add('events','View Events')
				->add('events/create','New Event'))

			->add('sermons','Sermons', Menu::items()
				->add('sermons','View Sermons')
				->add('sermons/create','New Sermon'))

			->add('slides','Slides', Menu::items()
				->add('slides','View Slides')
				->add('slides/create','New Slide'))

			->add('slogans', 'Slogans', Menu::items()
				->add("slogans/1/edit", 'Edit Slogan')) // Only one slogan so we cheat here

			->add('users','Users', Menu::items()
				->add('users','View Users')
				->add('users/create','New User'))

			->add('logout','Log out');
		$view->with('admin_menu',$admin_menu);
	}

	$view->with('slogan',Slogan::order_by('created_at')->first());

	if(isset($view->errors) && $view->errors->has())
	{
		$errorCount = count($errors = $view->errors->all('<li>:message</li>'));
		$msg = "<p>Please review the $errorCount issue(s) found:</p>\n<ul>\n";
		foreach($errors as $error) {
			$msg .= $error;
		}
		$msg .= "</ul>";

		Alert::add('error',$msg);
	}
});

View::composer('home', function($view)
{
	$view->with('sermons',Sermon::where('published','=',1)->order_by('preached_on','DESC')->take(1)->get());
	$view->with('blog',Feed::get_vvcf_blog());
	$view->with('events',Events::where('event_date','>=',new DateTime())->order_by('event_date', 'ASC')->take(2)->get());
	$view->with('tweets',Tweets::getTweets(1));
	$view->with('slides',Slide::all());
	$view->with('videos',Feed::get_vimeo_channel());
});

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application. The exception object
| that is captured during execution is then passed to the 500 listener.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function($exception)
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Route::get('/', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...

	Alert::serialize_messages();
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) {
		Alert::add('warning','You must be logged in to access this page.');
		return Redirect::to_action('user@login',401);
	}
	// if (Auth::guest()) return Redirect::to('login');
});