<?php

class Create_Slides_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//

		Schema::create('slides', function($table)
		{
			$table->engine = 'InnoDB';
		    $table->increments('id');
		    $table->string('path'); // The image's path
		    $table->string('url'); // The image's url
		    $table->string('link'); // A URL to link this image to
		    $table->string('heading'); // Title/heading of the image
		    $table->text('description'); // A description of this image
		    $table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//

		Schema::drop('slides');
	}

}