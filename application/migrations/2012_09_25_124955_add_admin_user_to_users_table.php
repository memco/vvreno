<?php

class Add_Admin_User_To_Users_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$user = new User;
		$user->username = 'Admin';
		$user->email = 'mikemckend@gmail.com';
		$user->password = '\:{,&-Jz#4]g\_CNq9QUMdua8*?Aw{';
		$user->save();
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		if ($user = User::find(1))
			$user->delete();
	}

}