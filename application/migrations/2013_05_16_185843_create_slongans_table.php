<?php

class Create_Slongans_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slogans', function($table)
		{
			$table->engine = 'InnoDB';
		    $table->increments('id');
		    $table->string('slogan');
		    $table->boolean('active');
		    $table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slogans');
	}

}