<?php

class Add_Mime_To_Sermons_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sermons', function($table)
		{
			$table->string('mime');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sermons', function($table)
		{
			$table->drop_column('mime');
		});
	}

}