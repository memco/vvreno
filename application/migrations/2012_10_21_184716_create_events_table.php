<?php

class Create_Events_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function($table)
		{
			$table->engine = 'InnoDB';

		    $table->increments('id');

		    // Event title
		    $table->string('title');

		    // Detailed description of the event
		    $table->text('description');

		    // Location of the event
		    $table->string('location');

		    // Date of event
		    $table->date('event_date');

		    $table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}