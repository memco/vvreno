<section class="messages">
	@foreach( array('success'=>'icon-ok-sign','error'=>'icon-exclamation-sign','warning'=>'icon-warning-sign','notice'=>'icon-info-sign') as $type=>$icon )
		@if( $messages->has( $type ))
			<div class="{{ $type }}">
				<h1 title="{{ Str::title( $type ) }}" class="{{ $icon}}"><span class="visually-hidden">{{ Str::title( $type ) }}</span></h1>
				@foreach( $messages->get( $type ) as $message )
					<p>{{ $message }}</p>
				@endforeach
			</div>
		@endif
	@endforeach
</section>