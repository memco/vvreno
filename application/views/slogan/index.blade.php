<h1>Slogans</h1>
@if(!empty($slogan))
<table class="data-list">
    <thead id="field-labels">
        <tr>
            <th>Slgoan</th>
            <th{{ Auth::user() ? ' colspan="3"' : '' }}>Last Updated</th>
        </tr>
    </thead>
    <tbody>
        <tr class="slogan field-data">
            <td class="slogan field">{{ $slogan->slogan }}</td>
            <td class="date-created field">{{ $slogan->updated_at->format('F j Y g:i A') }}</td>
            @if(Auth::user())
            <td>{{ HTML::link_to_action('slogan@edit', 'Edit', array($slogan->id)) }}</td>
            @endif
        </tr>
    </tbody>
</table>
@else
    <p><a href="{{ URL::to_action('slogan@add') }}" title="Create new slogan">Create new slogan</a>
@endif