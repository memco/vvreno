		<div class="row">

		<section id="main">
		@if(isset($slides))
			<section id="slideshow">
				<ul class="slides">
				@foreach($slides as $slide)
					<li>
						{{ $slide->link ? "<a href=\"$slide->link\">" : '' }}<img src="{{ $slide->get_url('727x320') }}">{{ $slide->link ? "</a>" : '' }}
						{{ $slide->heading ? "<h1>$slide->heading</h1>" : '' }}
						{{ $slide->description ? "<p>$slide->description</p>" : '' }}
					</li>
				@endforeach
				</ul>
			</section>
		@endif

		@if(isset($sermons))
			<section id="sermons">
				@foreach($sermons as $sermon)
					<div id="{{ $sermon->id }}">
						<h1 class="sermon-title">
							<span class="visually-hidden">Latest Sermon:</span>
							<a href="#{{ $sermon->id }}" class="icon-play listen-toggle" title="Listen to {{ $sermon->title }}">{{ $sermon->title }}</a>
						</h1>
						<p class="download"><a href="{{ $sermon->url }}" title="Download">Download <span data-icon="&#xf019"></span></a></p>
						<audio src="{{ $sermon->url }}" type="audio/mp3" controls preload="metadata"></audio>
					</div>
				@endforeach
				<p class="more-link"><a href="{{ URL::to('sermons') }}">Hear more sermons</a></p>
			</section>
		@endif

		<section id="vimeo">
			<h1>Videos</h1>
			@foreach($videos->get_items(0,1) as $video)
				<?php
					$url = $video->get_link();
					$url = explode('/', $url);
					$url = end($url);
				?>
				<div class="video-container" style="position: relative;padding-bottom: 75%; /* 16:9 */	padding-top: 25px;height: 0;">
					<iframe src="//player.vimeo.com/video/{{ $url }}" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			@endforeach
		</section>

		@if(isset($blog))
			<section id="blog">
				<h1>Blog</h1>
				@foreach($blog->get_items(0,1) as $entry)
					<header>
						<div class="blog-date">
							<p class="date-month">{{ $entry->get_date('M Y') }}</p>
							<p class="date-day">{{ $entry->get_date('j') }}</p>
						</div>

						<h2><a href="{{ $entry->get_permalink() }}">{{ $entry->get_title() }}</a></h2>
					</header>
					<article>
						<p>{{ Str::limit($entry->get_description(), 750, "…") }}</p>
					</article>
					<p class="more-link"><a href="{{ $entry->get_permalink() }}">Read More <span data-icon="&#xf0da"></span></a></p>
				@endforeach
			</section>
		@endif
		</section>

		<section id="sidebar">
			<section id="info">
				<h1>Join Us</h1>
				<h2>Sundays</h2>
				<ol>
					<li><span class="event-title">Food Pantry</span><span class="event-time"> 8:00 AM</span></li>
					<li><span class="event-title">Sunday School**</span><span class="event-time"> 9:15 AM</span></li>
					<li><span class="event-title">Worship Service**</span><span class="event-time"> 10:30 AM</span></li>
				</ol>

				<h2>Tuesdays</h2>
				<ol>
					<li><span class="event-title">Royal Rangers (Meets in Annex)</span><span class="event-time">6:30 PM</span></li>
					<li><span class="event-title">Bible Study</span><span class="event-time">6:30 PM</span></li>
				</ol>

				<h2>Wednesdays</h2>
				<ol>
					<li><span class="event-title">Tribe (High School)**</span><span class="event-time">6:30 PM</span></li>
				</ol>

				<h2>Thursdays</h2>
				<ol>
					<li><span class="event-title">Submerge (College, meets in annex)</span><span class="event-time">6:00 PM</span></li>
					<li><span class="event-title">Small Group: The Book of Romans</span><span class="event-time">6:30 PM</span><br>Call ahead for childcare. Meets in the sanctuary.</li>
				</ol>

				<h2>Saturdays</h2>
				<ol>
					<li><span class="event-title">Men's Breakfasts</span><span class="event-time">9:00 AM</span> (2nd Saturday)</li>
					<li><span class="event-title">Freedom's Road (Biker Service)</span><span class="event-time">5:30 PM</span></li>
				</ol>

				<p>** Childcare is provided.</p>

				<div class="location" title="location">
					<h2><span class="icon" aria-hidden="true" data-icon="&#xf041;"></span><span class="visually-hidden">Location</span></h2>
					<p><a href="https://maps.google.com/maps?q=Valley+View+Christian+Church,+Geiger+Grade+Road,+Reno,+NV&amp;hl=en&amp;sll=37.0625,-95.677068&amp;sspn=52.637906,106.259766&amp;oq=VAlley+view+christian+church+reno&amp;hq=Valley+View+Christian+Church,&amp;hnear=Geiger+Grade+Rd,+Reno,+Nevada+89521&amp;t=m&amp;z=14">1805 Geiger Grade Rd.<br>
					Reno NV, 89511</a></p>
				</div>
			</section>

		@if(isset($tweets))
			<section id="tweets">
				<h1>Tweets</h1>
				@foreach($tweets as $tweet)
				<p>{{ $tweet->text }} <span><a href="http://twitter.com/{{$tweet->user->screen_name}}/status/{{$tweet->id}}" class="twitter-time">{{ Feed::make_time_ago($tweet->created_at) }}</a></span></p>
				@endforeach
				<p class="more-link"><a href="https://twitter.com/theviewreno">See More Tweets <span data-icon="&#xf0da"></span></a></p>
			</section>
			@endif

			@if(isset($events) && ! empty($events))
			<section id="events">
				<h1>Events</h1>
				@foreach($events as $event)
					<div itemscope itemtype="http://schema.org/Event">
						<h2 itemprop="name"><a href="{{ URL::to_action('events@view', array($event->id)) }}">{{ $event->title }}</a></h2>
						{{ !empty($event->description) ? "<p>{$event->description}</p>" : '' }}
						{{ !empty($event->location) ? "<p>{$event->location}</p>" : '' }}
						<time itemprop="startDate" datetime="{{ $event->event_date->format('Y-m-d\Tg:i') }}">{{ $event->event_date->format('F j Y g:i A') }}</time>
					</div>
				@endforeach
				@if(Auth::check())
					<a href="{{ URL::to_action('events@add') }}" title="Add new event" class="button"><span data-icon="&#xf067"></span> New</a>
				@endif
			</section>
			@endif
		</section>