{{ Form::open(URI::current(), 'PUT') }}
<fieldset>
	<legend>Edit user</legend>
	{{ Form::token() }}
	<ol>
		<li>
			{{ Form::label('username','Username')}}
			{{ Form::text('username',Input::old('username',$user->username)) }}
		</li>
		<li>
			{{ Form::label('email','Email')}}
			{{ Form::email('email',Input::old('email',$user->email)) }}
		</li>
		<li>
			{{ Form::label('password','Password')}}
			{{ Form::password('password') }}
			<p class="input-advice">Leave blank to keep the same password</p>
		</li>
		<li>
			{{ Form::label('password_confirmation','Confirm Password')}}
			{{ Form::password('password_confirmation') }}
			<p class="input-advice">Please retype password (if changing).</p>
		</li>
		<li>
			{{ Form::submit('Save')}}
			<a href="{{ URL::to('users') }}">Cancel</a>
		</li>
	</ol>
</fieldset>
{{ Form::close() }}