
{{ Form::open() }}
<fieldset>
	<legend>Create user</legend>
	{{ Form::token() }}
	<ol>
		<li{{ $errors->has('username') ? ' class="error"' : '' }}>
			{{ Form::label('username','Username')}}
			{{ Form::text('username',Input::old('username')) }}
		</li>
		<li{{ $errors->has('email') ? ' class="error"' : '' }}>
			{{ Form::label('email','Email')}}
			{{ Form::email('email',Input::old('email')) }}
		</li>
		<li{{ $errors->has('password') ? ' class="error"' : '' }}>
			{{ Form::label('password','Password')}}
			{{ Form::text('password',Input::old('password')) }}
			<p class="input-advice">Leave blank to have a password created for you automatically (password will be shown to you after it is created). Passwrods will be saved securely.  You will not be able to view the password once saved.</p>
		</li>
		<li{{ $errors->has('password_confirmation') ? ' class="error"' : '' }}>
			{{ Form::label('password_confirmation','Confirm password')}}
			{{ Form::text('password_confirmation',Input::old('password_confirmation')) }}
			<p class="input-advice">Leave blank if using automatic password creation.</p>
		</li>
		<li>
			{{ Form::submit('Create user') }}
		</li>
	</ol>
</fieldset>
{{ Form::close() }}