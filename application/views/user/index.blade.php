<h1>Users</h1>
@if(! empty($users))
<table class="data-list">
	<thead>
		<tr>
			<th>Username</th>
			<th>Email</th>
			<th>Created on</th>
			<th>Last updated</th>
			@if(Auth::user())
				<th>Edit</th>
				<th>Delete</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr class="user">
			<td>{{ $user->username }}</td>
			<td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
			<td>{{ $user->created_at->format('F j Y g:ia') }}</td>
			<td>{{ $user->updated_at->format('F j Y g:ia') }}</td>
			@if(Auth::user() == $user OR Auth::user()->username == "admin")
				<td class="edit-link">{{ HTML::link_to_action('user@edit', 'Edit', array($user->id)) }}</td>
				<td class="delete-link">{{ HTML::link_to_action('user@delete', 'Delete', array($user->id)) }}</td>
			@endif
		</tr>
		@endforeach
	</tbody>
</table>
@else
	No users available at this time.  Please check back later.
@endif
<p><a href="{{ URL::to_action('user@add') }}" title="Create new user">Create new user</a>