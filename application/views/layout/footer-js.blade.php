		<!-- JQuery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{ URL::to_asset('libraries/mediaelement/jquery.js') }}"><\/script>')</script>

        <!-- JQuery UI-->
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>

        <!-- MediaElement -->
        {{ HTML::script('libraries/mediaelement/build/mediaelement-and-player.min.js')}}

        <!-- Flexslider -->
        {{ HTML::script('libraries/flexslider/jquery.flexslider-min.js') }}

        <!-- Timepicker -->
        {{ HTML::script('libraries/timepicker/jquery-ui-timepicker-addon.js') }}

        <!-- Analytics -->
        @if(Request::is_env('production'))
        <script defer>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-40502605-1', 'valleyviewreno.org');
              ga('send', 'pageview');
        </script>
        @endif

        <script defer type="text/javascript">
            $(document).ready( function() {

                // Mediaelement
               $('audio,video').mediaelementplayer({
                // width of audio player
                // audioWidth: '99%', // for some reason 100% causes volume slider to overflow
                // force iPad's native controls
                iPadUseNativeControls: true,
                // force iPhone's native controls
                iPhoneUseNativeControls: true,
                // force Android's native controls
                AndroidUseNativeControls: true,
                });

               // Date picker
               $('.date-picker, input[type="date"]').datepicker({
                    minDate: 0,
               });

               $('.datetime-picker, input[type="datetime"]').datetimepicker({
                    minDate: 0,
                    timeFormat: 'h:mm TT',
                    controlType: 'select'
               });

               // Slides
               $('#slideshow').flexslider();
            });
        </script>