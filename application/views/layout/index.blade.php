<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width">

        <title>{{ isset($title) ? "$title | " : '' }}Valley View Reno </title>
        {{ isset($pageDescription) ? '<meta name="description" content="'.$pageDescription.'">' : '' }}

        <!-- selectivzr -->

        {{-- <script async src="js/modernizr-2.6.1-respond-1.1.0.min.js"></script> --}}

        <link rel="icon" type="image/ico" href="{{ URL::to_asset('favicon.ico') }}">

        <link rel="stylesheet" href="{{ URL::to_asset('libraries/flexslider/flexslider.css') }}">
        <link rel="stylesheet" href="{{ URL::to_asset('libraries/timepicker/jquery-ui-timepicker-addon.css') }}">
        <link rel="stylesheet" href="{{ URL::to_asset('css/sass.css').'?'.File::modified(path('public').'css/sass.css') }}">
        {{-- HTML::style('libraries/mediaelement/mediaelementplayer.css') --}}

    </head>
    <body{{ Auth::Check() ? ' class="logged-in"' : ''}}>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->

        @if(isset($admin_menu))
            <nav id="menu-admin">
                {{ $admin_menu }}
            </nav>
        @endif

        <header id="site-header">
            <a href="{{ URL::to('/') }}"><img src="{{ URL::to_asset('img/logo-120x80.png') }}" alt="Valley View Christian Fellowship"></a>

            {{ isset($slogan) ? "<p id=\"slogan\">{$slogan->slogan}</p>" : '' }}

            <nav id="menu-social">
                <a href="https://plus.google.com/110816751180890279304/posts" title="Valley View Reno on Google+"><span aria-hidden="true" data-icon="&#xf0d4;"><span class="visually-hidden">Google+</span></a>
                <a href="https://twitter.com/theviewreno" title="Valley View Reno on Twitter"><span aria-hidden="true" data-icon="&#xf081;"></span><span class="visually-hidden">Twitter</span></a>
                <a href="https://www.facebook.com/vvcfreno" title="Valley View Reno on Facebook"><span aria-hidden="true" data-icon="&#xf082;"></span><span class="visually-hidden">Facebook</span></a>
                <a href="mailto:mail@valleyviewreno.org" title="Email Valley View"><span aria-hidden="true" data-icon="&#xe004;"></span><span class="visually-hidden">Email</span></a>
            </nav>
        </header>

        <?php
            $menu = Menu::handler('menu-site')
                                            ->add('about','About')
                                            ->add('sermons','Sermons')
                                            ->add('divorce-care', 'Divorce Care')
                                            ->add('celebrate-recovery', 'Celebrate Recovery')
                                            ->add('https://sites.google.com/a/valleyviewreno.org/tribe-valley-view-reno/','Tribe')
                                            ->add('http://dimensionsreno.org/','Dimensions')
                                            ->add('giving', 'Give')
        ?>

        <nav id="menu-site">
            {{ $menu }}
        </nav>

        {{ Alert::render('alerts.icomoon') }}

        <section id="content">
            {{ $content }}
        </section>

        <footer>
            <section id="contact-info">
                <p>
                1805 Geiger Grade Rd.<br />
                Reno NV, 89511<br />
                775-825-4099<br />
                <script type="text/javascript">document.write(
                    "<n uers=\"znvygb:znvy\100inyyrlivrjerab\056bet\">Trareny rznvy<\057n>".replace(/[a-zA-Z]/g, function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));
                </script><br />

                <script type="text/javascript">document.write(
                    "<n uers=\"znvygb:cnfgbewbua\100inyyrlivrjerab\056bet\">Cnfgbe Wbua'f rznvy<\057n>".replace(/[a-zA-Z]/g, function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));
                </script>
            </p>
            </section>
            <section class="site-navigation">
                {{ $menu->add('login','Log in') }}
            </section>
        </footer>

        {{ View::make('layout.footer-js') }}
    </body>
</html>