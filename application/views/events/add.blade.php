{{ Form::open() }}
	<fieldset>
		<legend></legend>
		{{ Form::token() }}
		<ol>
			<li>
				{{ Form::label('title','Title')}}
				{{ Form::text('title') }}
			</li>
			<li>
				{{ Form::label('description','Description')}}
				{{ Form::textarea('description') }}
			</li>
			<li>
				{{ Form::label('location','Location')}}
				{{ Form::text('location') }}
			</li>
			<li>
				{{ Form::label('event_date','Date')}}
				<input type="datetime" id="event_date" name="event_date">
			</li>
			<li>
				{{ Form::submit('Save') }}
			</li>
		</ol>
	</fieldset>
{{ Form::close() }}