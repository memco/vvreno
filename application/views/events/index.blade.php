<h1>Events</h1>
@if(!empty($events))
<table class="data-list">
    <thead>
        <tr>
            <th>Title</th>
            <th>description</th>
            <th>Location</th>
            <th>Date</th>
            @if(Auth::user())
                <th>Edit</th>
                <th>Delete</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
        <tr class="events">
            <td>{{ $event->title }}</a></td>
            <td>{{ $event->description }}</td>
            <td>{{ $event->location }}</td>
            <td>{{ $event->event_date->format('F j, Y g:i A') }}</td>

            @if(Auth::user())
                <td>{{ HTML::link_to_action('events@edit', 'Edit', array($event->id)) }}</td>
                <td>{{ HTML::link_to_action('events@delete', 'Delete', array($event->id)) }}</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
@else
    <p>No events available at this time.  Please check back later.</p>
@endif
@if(Auth::user())
    <p><a href="{{ URL::to_action('events@add') }}" title="Create new user">Create new event</a>
@endif