<h1>Sermons</h1>
@if(!empty($sermons->results))
{{ $sermons->links() }}
<table class="data-list">
	<thead>
		<tr>
			<th>Title</th>
			<th>Speakers</th>
			<th>Preached on</th>
			<th>Duration</th>
			<th>Size</th>
			@if(Auth::user())
				<th>Edit</th>
				<th>Delete</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach($sermons->results as $sermon)
		<tr class="sermon">
			<td><a href="{{ $sermon->url }}">{{ $sermon->title }}</a></td>
			<td>{{ $sermon->speakers }}</td>
			<td>{{ $sermon->preached_on->format('F j, Y') }}</td>
			<td>{{ $sermon->length->format('%H:%I:%S') }}</td>
			<td>{{ $sermon->size }}</td>
			@if(Auth::user())
				<td>{{ HTML::link_to_action('sermon@edit', 'Edit', array($sermon->id)) }}</td>
				<td>{{ HTML::link_to_action('sermon@delete', 'Delete', array($sermon->id)) }}</td>
			@endif
		</tr>
		@endforeach
	</tbody>
</table>
@else
	<p>No sermons available at this time.  Please check back later.</p>
@endif
@if(Auth::user())
	<p><a href="{{ URL::to_action('sermon@add') }}" title="Create new user">Add new sermon</a>
@endif