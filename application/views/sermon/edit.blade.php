{{ Form::open(URI::current(),'PUT') }}
	{{ Form::token() }}

	<fieldset>
		<legend>Sermon Details</legend>

		<ol>
			<li{{ $errors->has('title') ? ' class="error"' : '' }}>
				<label for="title">Title</label>
				{{ Form::text('title', Input::old('title',$sermon->title)) }}
			</li>
			<li{{ $errors->has('speakers') ? ' class="error"' : '' }}>
				<label for="speakers">Speakers</label>
				{{ Form::text('speakers', Input::old('speakers',$sermon->speakers)) }}
			</li>
			<li{{ $errors->has('preached_on') ? ' class="error"' : '' }}>
				<label for="preached_on">Preached on</label>
				{{ Form::date('preached_on', Input::old('preached_on',$sermon->preached_on->format('F j, Y'))) }}
			</li>
			<li{{ $errors->has('description') ? ' class="error"' : '' }}>
				<label for="description">Description</label>
				{{ Form::textarea('description', Input::old('description',$sermon->description)) }}
			</li>
			<li{{ $errors->has('published') ? ' class="error"' : '' }}>
				<label for="published">Published</label>
				{{ Form::checkbox('published', 1, Input::old('published',$sermon->published) == 1 ? true : false)}}
				<p class="input-advice"></p>
			</li>
			<li>
				{{ Form::submit('Save') }}
			</li>
		</ol>
	</fieldset>
{{ Form::close() }}