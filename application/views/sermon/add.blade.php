{{ Form::open_for_files() }}
<fieldset>
	<legend>Sermon Upload</legend>
	{{ Form::token() }}
	<ol>
		<li>
			{{ Form::label('file','File')}}
			{{ Form::file('file') }}
			<p class="input-advice">You can provide more info about this sermon on the next screen.</p>
		</li>
		<li>
			{{ Form::submit('Upload') }}
		</li>
	</ol>
</fieldset>
{{ Form::close() }}