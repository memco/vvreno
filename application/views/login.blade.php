<h1>Login</h1>
{{ Form::open() }}
    <ul>
        <li>
            {{ Form::label('username','Email') }}
            {{ Form::text('username',Input::old('username')) }}
        </li>
        <li>
            {{ Form::label('password','Password') }}
            {{ Form::password('password') }}
        </li>
        <li>
            {{ Form::submit('Login')}}
        </li>
    </ul>
{{ Form::close() }}