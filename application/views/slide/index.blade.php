<h1>Slides</h1>
@forelse($slides as $slide)
<div class="slide">
  <h1><a href="{{ $slide->url }}">{{ $slide->heading ?: str_replace('_',' ',pathinfo(parse_url($slide->url, PHP_URL_PATH), PATHINFO_FILENAME)) }}</a></h1>
  <p><img src="{{ $slide->get_url('345x215') }}"></p>
  <p>{{ $slide->description }}</p>
  <p>{{ $slide->link }}</p>
  @if(Auth::user())
    <div class="actions">
        <p>{{ HTML::link_to_action('slide@edit', 'Edit', array($slide->id)) }}</p>
        <p>{{ HTML::link_to_action('slide@delete', 'Delete', array($slide->id)) }}</p>
    </div>
  @endif
</div>
@empty
  No slides available at this time.  Please check back later.
@endforelse
<p id="add-link"><a href="{{ URL::to_action('slide@add') }}">Add new</a></p>