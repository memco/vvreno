<h1>Celebration Recovery</h1>
<div class="half">
	<h2>What is Celebrate Recovery?</h2>
	<p>Celebrate Recovery is a Christ-centered, 12-step based group to help people recover from their hurts, habits, and hang-ups. Weekly participation in a loving support group plays a vital role in people experiencing freedom from the destructive power of addictive and compulsive behaviors.</p>

	<h2>The Purpose of Celebrate Recovery</h2>
	<p>Celebrate Recovery seeks to fellowship and celebrate God’s healing power in our lives through the “8 Recovery Principles.” This experience allows us to “be changed.” By working and applying these biblical principles, we begin to grow spiritually. We become free from our addictive, compulsive, and dysfunctional behaviors. This freedom creates peace, serenity, joy, and, most importantly, a stronger personal relationship with God and others. As we progress through the program we discover our personal, loving, and forgiving Higher Power – Jesus Christ, the one and only true Higher Power.</p>

	<h2>Recovery Principles</h2>
	<p><a href="{{ URL::to_asset('documents/cr-8-principles-revised.pdf')}}">Rick Warren's 8 Recovery Principles</a></p>
</div>

<div class="half">
	<h2>Schedule</h2>
	<h4>5:00PM   Dinner – served until 5:45 pm</h4>
	<p>Cost: $5/person</p>

	<h4>5:50PM Free Childcare Available up to 12 years old</h4>

	<h4>6:00PM  Large Group Meeting – </h4>
	<p>Worship and then a teaching or testimony.</p>

	<h4>7:00PM  Open Share Groups (Small Groups)</h4>
	<p>Break into recovery specific groups. Men’s Chemical Dependency; Men’s Co-dependency and other issues; Women’s Chemical Dependency; Women’s Co-dependency and other issues.</p>

	<h4>8:00PM  Cross Talk</h4>
	<p>A great time of fellowship and dessert.</p>
</div>